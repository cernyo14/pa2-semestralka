Implementujte jednoduchou střílecí hru alespoň jednoho hráče proti počítači.

Hra musí splňovat následující funkcionality:

Objekty, které je možné zničit (nepřátelské jednotky, statické barikády)
Pohybovat s hráčem (chůze panáčka, let vesmírné lodi, páčka na odrážení míčků)
Po zničení objektu s určitou pravděpodobností zůstane bonus, implementujte několik různých bonusů (munice, rychlejší nabíjení,...)
Implementujte jednoduchý "fyzikální" model (setrvačnost lodi, gravitace, odrážení, nabíjení zbrani,...)
Implementujte načítání (generátoru) mapy ze souboru a ukládání nejlepších skóre do souboru.

Kde lze využít polymorfismus? (doporučené)

Rozdělení objektů: (ne)pohyblivý, hráč vs. nepřítel
Různé bonusy: munice, silnější zbraně, zdvojení zbraní, rychlejší přebíjení,...
Uživatelské rozhraní: konzole, ncurses, SDL, OpenGL (různé varianty), ...

Další informace

https://cs.wikipedia.org/wiki/Arkanoid
https://www.arcade-history.com/?n=ace-invaders&page=detail&id=115810
https://en.wikipedia.org/wiki/Asteroids_(video_game)
https://en.wikipedia.org/wiki/Tank_Battalion

--------------------
Moje implementace a ovladani:

Jednoducha strilecka ve 2D prostoru, ktera funguje tahove/krkove ( hra se aktulizuje po kazdem stisknuti klavesy ).

Nepratele jsou Y ( strilejici nepritel ) a y ( nestrilejici nepritel ).
Bonusy jsou reprezentavany jako b ( bud je jedna o brokovnici nebo o odstrelovaci pusku )
Hrac je reprezentovan jako X a ovlada se:
    w - pohyb nahoru
    s - pohyb dolu
    a - pohyb doleva
    d - pohyb doprava
    i - strelba nahoru
    k - strelba dolu
    j - strelba doleva
    l - strelba doprava

Hra skoci jakmile dojdou hraci zivoty nebo hrac dosahne skore tak blizkemu INT_MAX ze pri jakemkoliv dalsim killu by hrozilo preteceni integeru.
Po skonceni hry se hra zepta na jmeno a ulozi si skore.

Pro spusteni map testBonus.txt, testEnemyGun.txt, testEnemyNoGun.txt, zmente v constants.h promennou DEFAULT_MAP a zmente i promenou TESTMODE na true.
TESTMODE = true znemozni spawnovani dalsich Obejktu a neuklada score.

V deafaultni mape neni pouzit Object WoodenWall-rozbitelna zed strelbou ( problem byl ze AI nepritel neni dostatecne chytre ale nepratele zdi obesli ), je ovsem v testovaci mape testBonus.txt. 
