//
// Created by ondrej on 5/13/20.
//

#ifndef SEMESTRALKA_CONSTANTS_H
#define SEMESTRALKA_CONSTANTS_H

#include <stdint-gcc.h>

//constants that can be changed an it will not have effect on functionality of game ( game should not crash )

const int LIVES_DEFAULT = 100;
const bool TESTMODE = false;
const int START_SCORE = 0; //2147483647 - MAX_INT

const char * const DEFAULT_MAP = "examples/default.txt";
const uint32_t MAX_HEIGHT_MAP = 1000;
const uint32_t MAX_LENGTH_MAP = 1000;
const uint32_t MIN_HEIGHT_MAP = 10;
const uint32_t MIN_LENGTH_MAP = 20;

const char STATS_BOARD_TOP = '-';
const char STATS_BOARD_SIDES = '|';
const char * const SCORE_PRINT = "Score: ";
const char * const LIVES_PRINT = "HP: ";
const char * const WEAPON_PRINT = "Weapon: ";
const char * const GUN_TEXT = "Gun";
const char * const SNIPERGUN_TEXT = "Sniper gun";
const char * const SHOTGUN_TEXT = "Shotgun";

const char PLAYER_PRINT = 'X';
const char BULLET_PRINT = '*';
const char ENEMY_BULLET_PRINT = '+';
const char ENEMY_GUN_PRINT = 'Y';
const char ENEMY_NO_GUN_PRINT = 'y';
const char EMPTY_SPACE_PRINT = ' ';
const char WALL_PRINT = '#';
const char WOOD_PRINT = '@';
const char BOOST_PRINT = 'b';

const int ENEMY_WALK_DMG = 10;
const int ENEMY_BULLET_DMG = 10;
const int ENEMY_GUN_KILL = 50;
const int ENEMY_NO_GUN_KILL = 15;

const int MOVABLE_MOVES = 1;
const int PLAYER_MOVES = 1;
const int ENEMY_GUN_MOVES = 1;
const int ENEMY_NO_GUN_MOVES = 1;
const int BULLET_MOVES = 2;
const int SHOTGUN_BULLET_MOVES = 2;
const int SNIPER_BULLET_MOVES = 5;
const uint32_t SHOTGUN_BULLET_WIDTH = 3;

const int ENEMY_NO_GUN_BONUS_CHANCE = 4; //1 in 4 change of spawning bonus after killing enemy
const int ENEMY_GUN_BONUS_CHANCE = 2;

const int PLAYER_MOVE_UP = 119;     //w
const int PLAYER_MOVE_DOWN = 115;   //s
const int PLAYER_MOVE_LEFT = 97;    //a
const int PLAYER_MOVE_RIGHT = 100;  //d
const int PLAYER_SHOOT_UP = 105;    //i
const int PLAYER_SHOOT_DOWN = 107;  //k
const int PLAYER_SHOOT_LEFT = 106;  //j
const int PLAYER_SHOOT_RIGHT = 108; //l


const uint16_t ENEMY_COOLDOWN = 5;
const uint16_t BONUS_TIME = 20;

const int SPAWN_BONUS_RATE = 80;
const uint16_t SPAWN_BONUS_SPACE = 2;
const int SPAWN_BONUS_START = 3;
const int SPAWN_ENEMY_GUN_RATE = 60;
const int SPAWN_ENEMY_GUN_START = 1;
const uint16_t SPAWN_ENEMY_SPACE = 2;
const int SPAWN_ENEMY_NO_GUN_RATE = 20;
const int SPAWN_ENEMY_NO_GUN_START = 3;

const char * const CURSOR_TEXT = "-> ";
const char * const PLAY_TEXT = "Play Game";
const char * const HIGHSCORE_TEXT = "Show Scoreboard";
const char * const END_TEXT = "Exit Game";
const int CURSOR_UP = 119;      //w
const int CURSOR_DOWN = 115;    //s
const int CURSOR_CONFIRM = 10;  //'\n'

const char * const HIGHSCORE_FILE = "src/highscore.txt";
const int MAX_LENGTH_NAME = 20;
const char * const GAMEOVER_TEXT = "You Died.";
const char * const VICTORY_TEXT = "Congratulations, you have won!!!";
const char * const NAME_ENTER_TEXT = "Please enter your name and press Enter";
const char * const HIGHSCOREBOARD_TEXT = "#####  ###   ###      #    ###      ####  ####   ###   ###   ####  ####\n"
                                         "  #   #   #  #  #     #   #   #     #     #     #   #  #  #  #     #   \n"
                                         "  #   #   #  ###      #   #   #     ####  #     #   #  ###   ####  ####\n"
                                         "  #   #   #  #        #   #   #        #  #     #   #  # #   #        #\n"
                                         "  #    ###   #        #    ###      ####  ####   ###   #  #  ####  ####\n";
const char HIGHSCORE_BOARDER = '=';
const char * const POSITION_TEXT = "Position:";
const char * const SCORE_TEXT = "Score:";
const char * const NAME_TEXT = "Name:";
const char * const HIGHSCOREBOARD_END_TEXT = "Press escape to return to main menu";
const int HIGHSCOREBOARD_END_KEY = 27;
const int HIGHSCOREBOARD_SCORE_TABS = 3;
const int HIGHSCOREBOARD_NAME_TABS = 3;
const int NUMBER_OF_PRINTED_SCORES = 10;
const int NUMBER_OF_HELD_SCORES = 100;


//constants that should not be changed since they can effect functionality of game

const char PLAYER_REP = 'X';
const char BULLET_REP = '*';
const char ENEMY_GUN_REP = 'Y';
const char ENEMY_NO_GUN_REP = 'y';
const char EMPTY_SPACE_REP = ' ';
const char WALL_REP = '#';
const char WOOD_REP = '@';
const char SNIPER_REP = 'N';
const char SHOTGUN_REP = 'H';
const char SNIPER_BULLET_REP = 'n';
const char SHOTGUN_BULLET_REP = 'h';
const char ENEMY_BULLET_REP = 'e';

const int CURSOR_PLAY = 0;
const int CURSOR_HIGHSCORE = 1;
const int CURSOR_END = 2;

const int CONTINUE_GAME = 0;
const int LOST_GAME = 1;
const int WON_GAME = 2;

const int STAND_STILL = 0;
const int UP_DIR = 1;
const int DOWN_DIR = 2;
const int LEFT_DIR = 3;
const int RIGHT_DIR = 4;
const int SHOOT_UP = 5;
const int SHOOT_DOWN = 6;
const int SHOOT_LEFT = 7;
const int SHOOT_RIGHT = 8;

#endif //SEMESTRALKA_CONSTANTS_H
