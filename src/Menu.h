//
// Created by ondrej on 5/15/20.
//

#ifndef SEMESTRALKA_MENU_H
#define SEMESTRALKA_MENU_H

#include "constants.h"
#include "Input.h"
#include <iostream>

using namespace std;
/**
 * Represents game menu
 */
class Menu
{
    /**
     * To which position is cursor currently pointing at
     */
    int cursor;
    /**
     * Prints current state of Menu
     */
    void PrintMenu() const;
public:
    /**
     * Constructor
     * Sets cursors positions to Play Game position by default
     */
    Menu ( const int cursor = CURSOR_PLAY ) : cursor ( cursor ) { }
    /**
     * Runs Menu - Prints Menu, waits for input and based on input it moves cursor
     * When Enter is pressed it return current position of cursor
     * @return position of cursor when Enter is pressed
     */
    const int MainMenu();
};

#endif //SEMESTRALKA_MENU_H
