#include "Game.h"
#include "Menu.h"
#include "ScoreBoard.h"
#include <iostream>

using namespace std;

int main()
{
    Menu menu;
    while ( true )
    {
        switch ( menu.MainMenu() )
        {
            case CURSOR_END:
                return 0;
            case CURSOR_PLAY:
                if ( !Game().Play() )
                    return 1;
                break;
            case CURSOR_HIGHSCORE:
                if ( !ScoreBoard().Read() )
                    return 1;
                break;
            default:
                cout << "Something went wrong in main menu" << endl;
                return 1;
        }
    }
}