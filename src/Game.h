//
// Created by ondrej on 5/17/20.
//

#ifndef SEMESTRALKA_GAME_H
#define SEMESTRALKA_GAME_H
#include "Map.h"
#include "ScoreBoard.h"
#include <iostream>
/**
 * Represents game itself
 */
class Game
{
    /**
     * if testmode is true Game doesnt spawn any Object ( except for those which are loaded into Map from file which Map is loaded from )
     * if testmode is true it also doesnt save score ( made for testing )
     */
    bool testmode;
    /**
     * Counts Greatest Common Divisor of two given numbers
     * @return gcd
     */
    const int Gcd ( const int a, const int b );
    /**
     * Counts Least Common Multiple / Lowest Common Multiple of two given numbers
     * @return lcm
     */
    const int Lcm ( const int a, const int b );
public:
    /**
     * Constructor
     * @param testmode is true if I want play in testmode ( any Object do not spawn )
     */
    Game ( bool testmode = TESTMODE ) : testmode ( testmode ) { }
    /**
     * Method in which game runs ( starts game, spawns Objects, updates Objects on Map )
     * @return true if Player was killed or was about to achieved score of MAX_INT ( danger of int overflow )
     * @return false if error occurred ( i.e. file with scores or map was not found )
     */
    bool Play();
};

#endif //SEMESTRALKA_GAME_H
