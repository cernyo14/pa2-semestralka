//
// Created by ondrej on 5/18/20.
//

#include "ScoreBoard.h"
#include <fstream>
#include <algorithm>
#include <sstream>
#include <cstring>

int ScoreBoard::NumberOfDigits( int num ) const
{
    int digits = 0;
    if ( num == 0 )
        return 1;
    while ( num != 0 )
    {
        ++digits;
        num /= 10;
    }
    return digits;
}

const uint64_t ScoreBoard::GetLineLength( const char *text ) const
{
    size_t length = 0;
    for ( ; length < strlen( text ); ++length )
    {
        if ( text[length] == '\n' )
            return length;
    }
    return length;
}

bool ScoreBoard::Read()
{
    if ( !ReadAndSave() )
    {
        system( "clear" );
        cout << "Cannot read from file with scores or file does not exist" << endl;
        return false;
    }

    system( "clear" );
    //Prints text above scoreboard (TOP 10 SCORES)
    cout << HIGHSCOREBOARD_TEXT;
    for ( size_t i = 0; i < GetLineLength( HIGHSCOREBOARD_TEXT ); ++i )
        cout << HIGHSCORE_BOARDER;
    cout << endl;

    //Prints headline for positions, scores and names
    cout << POSITION_TEXT;

    for ( int i = 0; i < HIGHSCOREBOARD_SCORE_TABS; ++i )
        cout << '\t';
    cout << SCORE_TEXT;

    for ( int i = 0; i < HIGHSCOREBOARD_NAME_TABS; ++i )
        cout << '\t';
    cout << NAME_TEXT << endl;

    //Prints positions, scores and names
    for ( size_t i = 0; i < scoreboard.size(); ++i )
    {
        if ( i >= NUMBER_OF_PRINTED_SCORES )
            break;

        cout << i + 1 << ".";   //prints position

        for ( int i = 0; i < HIGHSCOREBOARD_SCORE_TABS + 1; ++i )
            cout << '\t';
        cout << scoreboard[i].first;    //prints score

        int numberOfNameTabs = HIGHSCOREBOARD_NAME_TABS;
        if ( NumberOfDigits( scoreboard[i].first) > 7 )
            --numberOfNameTabs;

        for ( int i = 0; i < numberOfNameTabs; ++i )
            cout << '\t';
        cout << scoreboard[i].second << endl;   //prints name
    }

    cout << HIGHSCOREBOARD_END_TEXT;
    bool escPressed = false;
    while ( !escPressed )
    {
        switch ( Input().GetInput() )
        {
            case HIGHSCOREBOARD_END_KEY:    //esc
                escPressed = true;
                break;
            default:
                break;
        }
    }
    return true;
}

const int ScoreBoard::StringToInt( string str )
{
    int result;
    istringstream( str ) >> result;
    return result;
}

bool ScoreBoard::ReadAndSave()
{
    ifstream sourceFile ( file );
    string line;
    string name;
    string score;
    score.clear();
    name.clear();

    if ( sourceFile.is_open() )
    {
        while ( getline( sourceFile, line ) )
        {
            bool readingScore = true;
            for ( auto it = line.begin(); it != line.end(); ++it )
            {
                if ( it == line.begin() && !isdigit( *it ) )
                {
                    return false;
                }

                if ( isdigit( *it ) && readingScore )
                {
                    score.push_back( *it );
                }
                else
                {
                    readingScore = false;
                    name.push_back( *it );
                }

            }
            scoreboard.push_back( make_pair( StringToInt( score ), name ) );
            score.clear();
            name.clear();
        }
        sourceFile.close();
        sort( scoreboard.begin(), scoreboard.end(), ScoresComparator() );
        return true;
    }
    return false;
}

string ScoreBoard::GetName( const char *text )
{
    char name[MAX_LENGTH_NAME];
    system( "clear" );
    cout << text << endl;
    cout << NAME_ENTER_TEXT << endl;
    cin.getline( name, sizeof( name ) );
    return name;
}

bool ScoreBoard::Add( const int score, const char * text )
{

    scoreboard.push_back( make_pair( score, GetName( text ) ) );
    if ( !ReadAndSave() )
        return false;

    ofstream sourceFile ( file );
    if ( sourceFile.is_open() )
    {
        sourceFile.open( file, ios::out | ios::trunc );
        sourceFile.clear();     // if file was empty trunc will set a failbit which would not allow us to write into file later on

        for ( size_t i = 0; i < scoreboard.size(); ++i )
        {
	        if ( i >= NUMBER_OF_HELD_SCORES )
		        break;
            sourceFile << scoreboard[i].first;
            sourceFile << scoreboard[i].second << endl;
        }
        sourceFile.close();
        return true;
    }
    return false;


}
