//
// Created by ondrej on 5/14/20.
//

#include <unistd.h>
#include "MovingObject.h"

void MovingObject::PlayerEnemy( int & lives )
{
    lives -= ENEMY_WALK_DMG;
}

void MovingObject::MovingEmptySpace( unique_ptr<Object> &moving, unique_ptr<Object> &empty )
{
    const uint32_t movingX = x;
    const uint32_t movingY = y;

    y = empty->GetY();
    x = empty->GetX();
    empty->SetY( movingY );
    empty->SetX( movingX );

    empty.swap( moving );
}


void MovingObject::PlayerBullet( unique_ptr<Object> &bullet, int &lives )
{
    const uint32_t emptyX = bullet->GetX();
    const uint32_t emptyY = bullet->GetY();
    bullet = move ( EmptySpace( emptyY, emptyX ).UniquePtr() );
    lives -= ENEMY_BULLET_DMG;
}

void MovingObject::ObjectDisappear( unique_ptr<Object> &object )
{
    const uint32_t emptyX = x;
    const uint32_t emptyY = y;
    object = move ( EmptySpace( emptyY, emptyX ).UniquePtr() );
}

void MovingObject::EnemyToBonus( unique_ptr<Object> &enemy )
{
    const uint32_t bonusX = enemy->GetX();
    const uint32_t bonusY = enemy->GetY();

    if ( rand() % 2 == 1 )
        enemy = move ( SniperGun( bonusY, bonusX ).UniquePtr() );
    else
        enemy = move ( ShotGun( bonusY, bonusX ).UniquePtr() );

}