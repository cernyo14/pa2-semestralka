//
// Created by ondrej on 5/14/20.
//

#ifndef SEMESTRALKA_ENEMYNOGUN_H
#define SEMESTRALKA_ENEMYNOGUN_H

#include "MovingObject.h"
/**
 * Represents enemy without a gun ( cannot shoot )
 */
class EnemyNoGun : public MovingObject
{
    /**
     * current Player y coordinates
     */
    uint32_t playerY;
    /**
     * current Player x coordinates
     */
    uint32_t playerX;
public:
    /**
     * Constructor
     * @param y sets EnemyNoGun y coordinates
     * @param x sets EnemyNoGun x coordinates
     * @param playerY sets playerY based on Player y coordinates
     * @param playerX sets playerX based on Player x coordinates
     */
    EnemyNoGun ( const uint32_t & y, const uint32_t & x , const uint32_t & playerY, const uint32_t & playerX )
        : MovingObject ( y, x, ENEMY_NO_GUN_REP, ENEMY_NO_GUN_MOVES ), playerY ( playerY ), playerX ( playerX ) { }

    unique_ptr < Object > UniquePtr () const override { return make_unique < EnemyNoGun > ( *this ); }

    void Print () const override { cout << ENEMY_NO_GUN_PRINT; }
    /**
     * Based on Player coordinates it return direction of EnemyNoGun
     * @return direction to which EnemyNoGun wants to move so it can get closer to Player
     */
    const int Dir () override;
    /**
     * Collision between EnemyNoGun and other Object
     * It changes given parameters based on with which Object EnemyNoGun collided
     * @param enemy pointer to EnemyNoGun position on Map
     * @param other pointer to other Object position on Map with which EnemyNoGun collided
     * @param score current score
     * @param lives current amount of Players lives ( its stored in class Map )
     * @param weapon currently equipped weapon
     */
    void Collision ( unique_ptr < Object > & enemy, unique_ptr < Object > & other, int & score, int & lives, string & weapon ) override;
    void SetPlayerX( const uint32_t & X ) override { playerX = X; }
    void SetPlayerY( const uint32_t & Y ) override { playerY = Y; }
};

#endif //SEMESTRALKA_ENEMYNOGUN_H
