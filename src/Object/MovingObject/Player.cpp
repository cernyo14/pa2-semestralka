//
// Created by ondrej on 5/14/20.
//

#include "Player.h"

void Player::PlayerBonus( unique_ptr<Object> &player, unique_ptr<Object> &bonus )
{
    bonusTime = BONUS_TIME;
    const uint32_t playerY = y;
    const uint32_t playerX = x;

    y = bonus->GetY();
    x = bonus->GetX();

    player.swap( bonus );
    player = move( EmptySpace( playerY, playerX ).UniquePtr() );
}

const int Player::Dir()
{
    if ( bonusTime != 0 )
        --bonusTime;

    switch ( Input().GetInput() )
    {
        case PLAYER_MOVE_UP :       //w
            return UP_DIR;
        case PLAYER_MOVE_DOWN :     //s
            return DOWN_DIR;
        case PLAYER_MOVE_LEFT:      //a
            return LEFT_DIR;
        case PLAYER_MOVE_RIGHT :    //d
            return RIGHT_DIR;
        case PLAYER_SHOOT_UP :      //i
            return SHOOT_UP;
        case PLAYER_SHOOT_DOWN :    //j
            return SHOOT_DOWN;
        case PLAYER_SHOOT_LEFT :    //k
            return SHOOT_LEFT;
        case PLAYER_SHOOT_RIGHT :   //l
            return SHOOT_RIGHT;
        default:
            return STAND_STILL;
    }
}

void Player::Collision( unique_ptr<Object> &player, unique_ptr<Object> &other, int &score, int &lives, string &weapon )
{
    if ( bonusTime == 0 )
        weapon = GUN_TEXT;

    switch ( other->Rep() )
    {
        case EMPTY_SPACE_REP:
            MovingEmptySpace( player, other );
            break;
        case ENEMY_GUN_REP:
            PlayerEnemy( lives );
            break;
        case ENEMY_NO_GUN_REP:
            PlayerEnemy( lives );
            break;
        case SNIPER_REP:
            PlayerBonus( player, other );
            weapon = SNIPERGUN_TEXT;
            break;
        case SHOTGUN_REP:
            PlayerBonus( player, other );
            weapon = SHOTGUN_TEXT;
            break;
        case ENEMY_BULLET_REP:
            PlayerBullet( other, lives );
            break;
        case SNIPER_BULLET_REP:
            ObjectDisappear( other );
            break;
        case SHOTGUN_BULLET_REP:
            ObjectDisappear( other );
            break;
        case BULLET_REP:
            ObjectDisappear( other );
            break;
        default:
            break;
    }
}

