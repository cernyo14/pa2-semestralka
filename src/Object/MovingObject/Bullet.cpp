//
// Created by ondrej on 5/14/20.
//

#include "Bullet.h"
#include "../StationaryObject/EmptySpace.h"
#include <unistd.h>

void Bullet::Collision( unique_ptr<Object> &bullet, unique_ptr<Object> &other, int &score, int &lives, string &weapon)
{
    switch ( other->Rep() )
    {
        case EMPTY_SPACE_REP:
            MovingEmptySpace( bullet, other );
            break;
        case WALL_REP:
            ObjectDisappear( bullet );
            break;
        case ENEMY_GUN_REP:
            ObjectDisappear( bullet );
            if ( rep != ENEMY_BULLET_REP )
            {
                score += ENEMY_GUN_KILL;
                if ( rand() % ENEMY_GUN_BONUS_CHANCE == 1 )
                    EnemyToBonus( other );
                else
                    ObjectDisappear( other );
            }
            break;
        case ENEMY_NO_GUN_REP:
            ObjectDisappear( bullet );
            if ( rep != ENEMY_BULLET_REP )
            {
                score += ENEMY_GUN_KILL;
                if ( rand() % ENEMY_NO_GUN_BONUS_CHANCE == 1 )
                    EnemyToBonus( other );
                else
		            ObjectDisappear( other );
                   
            }
            break;
        case PLAYER_REP:
            if ( rep == ENEMY_BULLET_REP )
            {
                PlayerBullet( bullet, lives );
                break;
            }
            ObjectDisappear( bullet );
            break;
        default:
            ObjectDisappear( bullet );
            ObjectDisappear( other );
            break;

    }
}
