//
// Created by ondrej on 5/14/20.
//

#ifndef SEMESTRALKA_PLAYER_H
#define SEMESTRALKA_PLAYER_H

#include "MovingObject.h"
#include "../StationaryObject/EmptySpace.h"
#include "../StationaryObject/ShotGun.h"
#include "../StationaryObject/SniperGun.h"
#include "../StationaryObject/StoneWall.h"
#include "../StationaryObject/WoodenWall.h"
#include "../../Input.h"
#include "EnemyGun.h"
#include "EnemyNoGun.h"
/**
 * Represents Object you are playing as
 */
class Player : public MovingObject
{
    /**
     * Shows for how many more game ticks will Player have bonus gun ( SniperGun or ShotGun ) equipped
     */
    uint16_t bonusTime;
    /**
     * This method is called when Player has a Collision with bonus item ( SniperGun or ShotGun )
     * It sets Players position to where bonus item position was, swaps Player and bonus ptr and sets ptr to bonus item to ptr to EmptySpace
     * @param player is pointer to Player
     * @param bonus is pointer to bonus item ( SniperGun or ShotGun )
     */
    void PlayerBonus ( unique_ptr < Object > & player, unique_ptr < Object > & bonus );

public:
    /**
     * Constructor
     * @param y sets y coordinates
     * @param x sets x coordinates
     */
    Player ( const uint32_t & y, const uint32_t & x ) : MovingObject ( y, x, PLAYER_REP, PLAYER_MOVES ), bonusTime ( 0 ) { }

    unique_ptr < Object > UniquePtr () const override { return make_unique < Player > ( *this ); }

    void Print () const override { cout << PLAYER_PRINT; }
    /**
     * gets input from keyboard using class Input
     * @return to which direction will Player move or shoot
     */
    const int Dir () override;
    /**
     * Collision between Player and other Object
     * It changes given parameters based on with which Object Player collided
     * @param player pointer to Players position on map
     * @param other pointer to Object with which Player collided
     * @param score current game score
     * @param lives current amount of Players lives ( its stored in class Map )
     * @param weapon currently equipped weapon
     */
    void Collision ( unique_ptr < Object > & player, unique_ptr < Object > & other, int & score, int & lives, string & weapon ) override;
};

#endif //SEMESTRALKA_PLAYER_H
