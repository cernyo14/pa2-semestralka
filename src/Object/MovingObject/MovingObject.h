//
// Created by ondrej on 5/14/20.
//

#ifndef SEMESTRALKA_MOVINGOBJECT_H
#define SEMESTRALKA_MOVINGOBJECT_H

#include "../Object.h"
#include "../StationaryObject/EmptySpace.h"
#include "../StationaryObject/ShotGun.h"
#include "../StationaryObject/SniperGun.h"

/**
 * Abstract class representing moving Object
 */
class MovingObject : public Object
{
protected:
    /**
     * this method is called when there is Collision between Player and Enemy ( EnemyNoGun and EnemyGun )
     * it subtracts from Player lives
     * @param lives is current amount of Players lives
     */
    void PlayerEnemy ( int & lives );
    /**
     * Collision between any MovingObject and EmptySpace
     * It swaps position of both MovingObject and EmptySpace and also swaps pointers to their positions on map
     * @param moving is pointer to where MovingObject on map was
     * @param empty is pointer to where EmptySpace on map was
     */
    void MovingEmptySpace ( unique_ptr < Object > & moving, unique_ptr < Object > & empty );
    /**
     * Collision between Player and Bullet from EnemyGun
     * it subtracts from Player lives
     * @param bullet is pointer to where Bullet on map was
     * @param lives is current amount of Player lives
     */
    void PlayerBullet ( unique_ptr < Object > & bullet, int & lives );
    /**
     * Sets Object which should disappear to EmptySpace
     * @param object is Object which should disappear
     */
    void ObjectDisappear ( unique_ptr < Object > & object );
    /**
     * Sets enemy Object ( EnemyGun or EnemyNoGun ) to bonus Object ( SniperGun or ShotGun )
     * @param enemy enemy which was shot down
     */
    void EnemyToBonus ( unique_ptr < Object > & enemy );
public:
    /**
     * Constructor
     * @param y sets y coordinates
     * @param x sets x coordinates
     * @param rep sets MovingObject char representation
     * @param moves sets how many moves MovingObject does in one game tick
     */
    MovingObject ( const uint32_t & y, const uint32_t & x, const char rep, const int moves = MOVABLE_MOVES ) : Object ( y, x, rep, moves ) { }

    void SetPlayerX( const uint32_t & X ) override { }

    void SetPlayerY( const uint32_t & Y ) override { }

};

#endif //SEMESTRALKA_MOVINGOBJECT_H
