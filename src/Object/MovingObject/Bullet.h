//
// Created by ondrej on 5/14/20.
//

#ifndef SEMESTRALKA_BULLET_H
#define SEMESTRALKA_BULLET_H

#include "MovingObject.h"
/**
 * Represents bullet
 */
class Bullet : public MovingObject
{
    /**
     * Direction of Bullet movement ( it stays the same since it leaves its owner )
     */
    const int dir;
public:
    /**
     * Constructor
     * @param y sets Bullet y coordinates
     * @param x sets Bullet x coordinates
     * @param dir sets Bullet direction of movement
     * @param rep char representation of Bullet it is different for different types of Bullet ( default Player Bullet, Enemy Bullet, Bullet from ShotGun, etc. )
     * @param moves sets how many moves Object does in one game tick ( how many fields on Map it travels in one game tick )
     */
    Bullet ( const uint32_t & y, const uint32_t & x, const int & dir , const char rep = BULLET_REP, const int moves = BULLET_MOVES )
        : MovingObject ( y, x, rep, moves ), dir ( dir ) { }

    unique_ptr < Object > UniquePtr () const override { return make_unique < Bullet > ( *this ); }

    void Print () const override
    {
        if ( rep == ENEMY_BULLET_REP )
            cout << ENEMY_BULLET_PRINT;
        else
            cout << BULLET_PRINT;
    }

    const int Dir () override { return dir; }
    /**
     * Collision between Bullet and other Object
     * It changes given parameters based on with which Object Bullet collided and also based on Bullet representations
     * @param bullet pointer to Bullet position on Map
     * @param other pointer to other Object position on Map with which Bullet collided
     * @param score current score
     * @param lives current amount of Player lives ( its stored in class Map )
     * @param weapon currently equipped weapon
     */
    void Collision ( unique_ptr < Object > & bullet, unique_ptr < Object > & other, int & score, int & lives, string & weapon ) override;
};

#endif //SEMESTRALKA_BULLET_H
