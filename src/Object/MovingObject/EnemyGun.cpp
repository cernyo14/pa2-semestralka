//
// Created by ondrej on 5/14/20.
//

#include <unistd.h>
#include "EnemyGun.h"
void EnemyGun::Collision( unique_ptr < Object > &enemy, unique_ptr<Object> &other, int &score, int &lives, string &weapon )
{
    switch ( other->Rep() )
    {
        case EMPTY_SPACE_REP:
            MovingEmptySpace( enemy, other );
            break;
        case PLAYER_REP:
            PlayerEnemy( lives );
            break;
        case SNIPER_REP:
            ObjectDisappear( other );
            MovingEmptySpace( enemy, other );
            break;
        case SHOTGUN_REP:
            ObjectDisappear( other );
            break;
        case ENEMY_BULLET_REP:
            ObjectDisappear( other );
            break;
        case SNIPER_BULLET_REP:
            score += ENEMY_GUN_KILL;
            ObjectDisappear( other );
            if ( rand() % ENEMY_GUN_BONUS_CHANCE == 1 )
                EnemyToBonus( enemy );
            else
                ObjectDisappear( enemy );
            break;
        case SHOTGUN_BULLET_REP:
            score += ENEMY_GUN_KILL;
            ObjectDisappear( other );
            if ( rand() % ENEMY_GUN_BONUS_CHANCE == 1 )
                EnemyToBonus( enemy );
            else
                ObjectDisappear( enemy );
            break;
        case BULLET_REP:
            score += ENEMY_GUN_KILL;
            ObjectDisappear( other );
            if ( rand() % ENEMY_GUN_BONUS_CHANCE == 1 )
                EnemyToBonus( enemy );
            else
                ObjectDisappear( enemy );
            break;
        default:
            break;
    }
}

const int EnemyGun::Shoot( const uint32_t & Ydiff,  const uint32_t & Xdiff, bool left, bool up )
{
    cooldown = ENEMY_COOLDOWN;
    if ( Ydiff == 0 )
    {
        if ( left )
            return SHOOT_LEFT;
        return SHOOT_RIGHT;
    }
    else if ( Xdiff == 0 )
    {
        if ( up )
            return SHOOT_UP;
        return SHOOT_DOWN;
    }
    return STAND_STILL; //if I get here something went wrong so i rather do not move
}

const int EnemyGun::Dir()
{
    uint32_t Xdiff = 0;
    uint32_t Ydiff = 0;
    bool left = false;
    bool up = false;

    if ( x > playerX )
    {
        Xdiff = x - playerX;
        left = true;
    }
    else if ( x < playerX )
        Xdiff = playerX - x;

    if ( y > playerY )
    {
        Ydiff = y - playerY;
        up = true;
    }
    else if ( y < playerY )
        Ydiff = playerY - y;

    if ( cooldown == 0 )
    {
        if ( ( Xdiff == 0 || Ydiff == 0 ) )
            return Shoot(Ydiff, Xdiff, left, up);
    }
    else
    {
        --cooldown;
        if ( Ydiff > Xdiff )
        {
            if ( left )
                return RIGHT_DIR;
            return LEFT_DIR;
        }
        else
        {
            if ( up )
                return DOWN_DIR;
            return UP_DIR;
        }
    }


    if ( Ydiff > Xdiff )
    {
        if ( left )
            return LEFT_DIR;
        return RIGHT_DIR;
    }
    else
    {
        if ( up )
            return UP_DIR;
        return DOWN_DIR;
    }
}
