//
// Created by ondrej on 5/14/20.
//

#ifndef SEMESTRALKA_ENEMYGUN_H
#define SEMESTRALKA_ENEMYGUN_H

#include "MovingObject.h"
/**
 *  Represents enemy with a gun ( can shoot )
 */
class EnemyGun : public MovingObject
{
    /**
    * current Player y coordinates
    */
    uint32_t playerY;
    /**
    * current Player x coordinates
    */
    uint32_t playerX;
    /**
     * For how many game tick EnemyGun is not allowed to shoot ( how many game tick until the end of reloading )
     */
    uint16_t cooldown;
public:
    /**
    * Constructor
    * @param y sets EnemyGun y coordinates
    * @param x sets EnemyGun x coordinates
    * @param playerY sets playerY based on current Player y coordinates
    * @param playerX sets playerX based on current Player x coordinates
    */
    EnemyGun ( const uint32_t & y, const uint32_t & x, const uint32_t & playerY, const uint32_t & playerX )
            : MovingObject ( y, x, ENEMY_GUN_REP, ENEMY_GUN_MOVES ), playerY ( playerY ), playerX ( playerX ), cooldown ( 0 ) { }

    unique_ptr < Object > UniquePtr () const override { return make_unique < EnemyGun > ( *this ); }

    void Print () const override { cout << ENEMY_GUN_PRINT; }
    /**
     * To which direction will EnemyGun shoot
     * @param Ydiff difference between player y coordinates and EnemyGun y coordinates
     * @param Xdiff difference between player x coordinates and EnemyGun x coordinates
     * @param left is true when Player is on the left side from EnemyGun ( Player has bigger x coordinates )
     * @param up is true when Player is above from EnemyGun ( Player has smaller y coordinates )
     * @return to which direction will EnemyGun shoot
     */
    const int Shoot ( const uint32_t & Ydiff,  const uint32_t & Xdiff, bool left, bool up );
    /**
     * To which direction will EnemyGun move or shoot
     * If Player x or y coordinates match with EnemyGun coordinates it calls Shoot method
     * If either coordinates do not match and cooldown is 0 is returns direction of movement so it can match Player x or y coordinates
     * If cooldown is not 0 it returns a direction so it can get away from Player ( since it cannot shoot )
     * @return direction of movement or shooting
     */
    const int Dir () override;
    /**
     * Collision between EnemyGun and other Object
     * It changes given parameters based on with which Object EnemyGun collided
     * @param enemy pointer to EnemyGun position on Map
     * @param other pointer to other Object position on Map with which EnemyGun collided
     * @param score current score
     * @param lives current amount of Player lives ( its stored in class Map )
     * @param weapon currently equipped weapon
     */
    void Collision ( unique_ptr < Object > & enemy, unique_ptr < Object > & other, int & score, int & lives, string & weapon ) override;
    void SetPlayerX( const uint32_t & X ) override { playerX = X; }
    void SetPlayerY( const uint32_t & Y ) override { playerY = Y; }

    };

#endif //SEMESTRALKA_ENEMYGUN_Hs
