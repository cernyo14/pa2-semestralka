//
// Created by ondrej on 5/14/20.
//

#ifndef SEMESTRALKA_STATIONARYOBJECT_H
#define SEMESTRALKA_STATIONARYOBJECT_H

#include "../Object.h"
/**
 * Abstract class which represents Objects that cannot move
 */
class StationaryObject : public Object
{
public:
    StationaryObject ( const uint32_t & y, const uint32_t & x, const char rep ) : Object ( y, x, rep, STAND_STILL ) { }
    const int Dir () override { return STAND_STILL; }
    void Collision ( unique_ptr < Object > & This, unique_ptr < Object > & Other, int & score, int & lives, string & weapon ) override { }
    void SetPlayerX( const uint32_t & X ) override { }
    void SetPlayerY( const uint32_t & Y ) override { }
};


#endif //SEMESTRALKA_STATIONARYOBJECT_H
