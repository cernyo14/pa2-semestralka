//
// Created by ondrej on 5/14/20.
//

#ifndef SEMESTRALKA_STONEWALL_H
#define SEMESTRALKA_STONEWALL_H

#include "StationaryObject.h"
/**
 * Represents indestructible wall
 */
class StoneWall : public StationaryObject
{
public:
    StoneWall ( const uint32_t & y, const uint32_t & x ) : StationaryObject ( y, x, WALL_REP ) { }
    unique_ptr < Object > UniquePtr () const override { return make_unique < StoneWall > ( *this ); }
    void Print () const override { cout << WALL_PRINT; }
};

#endif //SEMESTRALKA_STONEWALL_H
