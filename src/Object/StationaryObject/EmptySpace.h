//
// Created by ondrej on 5/14/20.
//

#ifndef SEMESTRALKA_EMPTYSPACE_H
#define SEMESTRALKA_EMPTYSPACE_H

#include "StationaryObject.h"
/**
 * Represents empty space
 */
class EmptySpace : public StationaryObject
{
public:
    EmptySpace ( const uint32_t & y, const uint32_t & x ) : StationaryObject ( y, x, EMPTY_SPACE_REP ) { }
    unique_ptr < Object > UniquePtr () const override { return make_unique < EmptySpace > ( *this ); }
    void Print () const override { cout << EMPTY_SPACE_PRINT; }
};

#endif //SEMESTRALKA_EMPTYSPACE_H
