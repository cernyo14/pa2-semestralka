//
// Created by ondrej on 5/14/20.
//

#ifndef SEMESTRALKA_SNIPERGUN_H
#define SEMESTRALKA_SNIPERGUN_H

#include "StationaryObject.h"
/**
 * Bonus Object, when Player picks this up he has sniper gun equipped for limited amount of game ticks
 */
class SniperGun : public StationaryObject
{
public:
    SniperGun ( const uint32_t & y, const uint32_t & x ) : StationaryObject ( y, x, SNIPER_REP ) { }
    unique_ptr < Object > UniquePtr () const override { return make_unique < SniperGun > ( *this ); }
    void Print () const override { cout << BOOST_PRINT; }
};

#endif //SEMESTRALKA_SNIPERGUN_H
