//
// Created by ondrej on 5/14/20.
//

#ifndef SEMESTRALKA_SHOTGUN_H
#define SEMESTRALKA_SHOTGUN_H

#include "StationaryObject.h"
/**
* Bonus Object, when Player picks this up he has shotgun equipped for limited amount of game ticks
*/
class ShotGun : public StationaryObject
{
public:
    ShotGun ( const uint32_t & y, const uint32_t & x ) : StationaryObject ( y, x, SHOTGUN_REP ) { }
    unique_ptr < Object > UniquePtr () const override { return make_unique < ShotGun > ( *this ); }
    void Print () const override { cout << BOOST_PRINT; }
};

#endif //SEMESTRALKA_SHOTGUN_H
