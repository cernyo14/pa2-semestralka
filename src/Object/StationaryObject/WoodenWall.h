//
// Created by ondrej on 5/14/20.
//

#ifndef SEMESTRALKA_WOODENWALL_H
#define SEMESTRALKA_WOODENWALL_H

#include "StationaryObject.h"
/**
 * Represents destructible wall
 */
class WoodenWall : public StationaryObject
{
public:
    WoodenWall ( const uint32_t & y, const uint32_t & x ) : StationaryObject( y, x, WOOD_REP ) { }
    unique_ptr < Object > UniquePtr () const override { return make_unique < WoodenWall > ( *this ); }
    void Print () const override { cout <<  WOOD_PRINT; }
};

#endif //SEMESTRALKA_WOODENWALL_H
