//
// Created by ondrej on 5/13/20.
//

#ifndef SEMESTRALKA_OBJECT_H
#define SEMESTRALKA_OBJECT_H

#include "../constants.h"
#include <iostream>
#include <memory>
#include <vector>

using namespace std;
/**
 * Represents Object on Map
 */
class Object
{
protected:
    /**
     * y coordinates
     */
    uint32_t y;
    /**
     * x coordinates
     */
    uint32_t x;
    /**
     * by which char is Object represented ( unique to different types of Objects )
     * also does not need to equal to how Object is printed
     */
    const char rep;
    /**
     * how many moves does Object do in one game tick ( how many fields on Map it travels in one game tick )
     */
    int moves;

public:
    /**
     * Constructor
     * @param y is y coordinate, where Object is placed on Map
     * @param x is x coordinate, where Object is placed on Map
     * @param rep sets char representation of Object
     * @param moves sets how many moves Object does in one game tick ( how many fields on Map it travels in one game tick )
     */
    Object ( const uint32_t & y, const uint32_t & x, const char rep, const int moves ) : y ( y ), x ( x ), rep ( rep ), moves ( moves ) { }
    /**
     * Destructor
     */
    virtual ~Object() = default;
    /**
     * @return unique_ptr on Object
     */
    virtual unique_ptr < Object > UniquePtr () const = 0;
    /**
     * Prints out Object
     */
    virtual void Print () const = 0;
    /**
     * Direction
     * @return direction in which the Object is going to move
     */
    virtual const int Dir () = 0;
    /**
     * Getter to char representation of Object
     * @return char rep
     */
    const char & Rep () const { return rep; }
    /**
     * Getter to moves
     * @return moves
     */
    const int & Moves () const { return moves; }
    /**
     * Collision between two Object
     * It changes its parameters based on which two Objects collided
     * @param This is pointer to Object on Map which is trying to move, after collision pointer to this Object can be changed to point at a different Object
     * ( for example This is pointer to Bullet and collides with pointer to StoneWall ( Other ), then This will be pointing to EmptySpace instead of Bullet )
     * @param Other is pointer to Object on Map to which This collided into
     * @param score current score
     * @param lives how many lives is left ( its stored in class Map )
     * @param weapon which weapon is now equipped
     */
    virtual void Collision ( unique_ptr < Object > & This, unique_ptr < Object > & Other, int & score, int & lives, string & weapon ) = 0;
    /**
     * Getter to x
     * @return x coordinates
     */
    const uint32_t & GetX() const { return x; }
    /**
     * Getter to y
     * @return y coordinates
     */
    const uint32_t & GetY() const { return y; }
    /**
     * Setter to x
     * @param X is what x coordinates will be set to
     */
    void SetX( const uint32_t & X ) { x = X; }
    /**
     * Setter to y
     * @param Y is what y coordinates will be set to
     */
    void SetY( const uint32_t & Y ) { y = Y; }
    /**
     * Setter to Players x coordinates inside Object which need to know Players position
     * This method is only implemented in EnemyNoGun and EnemyGun,
     * since they need Players position to know in which direction to move
     * @param X is what Player x coordinates are currently equal to
     */
    virtual void SetPlayerX( const uint32_t & X ) = 0;
    /**
    * Setter to Players y coordinates inside Object which need to know Players position
    * This method is only implemented in EnemyNoGun and EnemyGun,
    * since they need Players position to know in which direction to move
    * @param Y is what Player y coordinates are currently equal to
    */
    virtual void SetPlayerY( const uint32_t & Y ) = 0;
    };

#endif //SEMESTRALKA_OBJECT_H
