//
// Created by ondrej on 5/15/20.
//

#include "./Menu.h"

void Menu::PrintMenu() const
{
    if ( cursor == CURSOR_PLAY )
        cout << CURSOR_TEXT;
    cout << PLAY_TEXT << endl;

    if ( cursor == CURSOR_HIGHSCORE )
        cout << CURSOR_TEXT;
    cout << HIGHSCORE_TEXT << endl;

    if ( cursor == CURSOR_END )
        cout << CURSOR_TEXT;
    cout << END_TEXT << endl;
}

const int Menu::MainMenu()
{
    bool end = false;
    system( "clear" );
    PrintMenu();
    while ( !end )
    {
        switch ( Input().GetInput() )
        {
            case CURSOR_UP:         //w
                --cursor;
                if ( cursor < CURSOR_PLAY )
                    cursor = CURSOR_END;
                break;
            case CURSOR_DOWN:       //s
                ++cursor;
                if ( cursor > CURSOR_END )
                    cursor = CURSOR_PLAY;
                break;
            case CURSOR_CONFIRM:    //'\n'
                end = true;
                break;
            default:
                break;
        }
        system( "clear" );
        PrintMenu();
    }
    return cursor;
}

