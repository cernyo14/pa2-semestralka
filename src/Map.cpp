//
// Created by ondrej on 5/13/20.
//

#include "./Map.h"
#include <iostream>
#include <limits>
#include <fstream>
#include <cstring>
#include <unistd.h>
#include <sys/ioctl.h>

Map::Map ( const char * source )
    : height ( 0 ),
      width ( 0 ),
      score ( START_SCORE ),
      lives ( LIVES_DEFAULT ),
      weapon ( GUN_TEXT ),
      playerX ( 0 ),
      playerY ( 0 ),
      valid ( true )
{
    string line;
    uint32_t lineNum = 0;
    bool firstLine = true;
    ifstream sourceFile ( source );
    if ( sourceFile.is_open() )
    {
        while ( getline ( sourceFile, line ) )
        {
            if ( ( !firstLine && ( width != line.size() ) ) || line.size() > MAX_LENGTH_MAP || line.size() < MIN_LENGTH_MAP )
                valid = false;
            ++height;
            map.push_back( vector < unique_ptr < Object > >() );
            for ( size_t i = 0; i < line.size(); ++i )
            {
                char c = line[i];
                switch ( c )
                {
                    case PLAYER_REP:
                        map[lineNum].push_back( Player( height - 1, i ).UniquePtr() );
                        playerY = height - 1;
                        playerX = i;
                        break;
                    case EMPTY_SPACE_REP:
                        map[lineNum].push_back( EmptySpace( height - 1, i ).UniquePtr() );
                        break;
                    case WALL_REP:
                        map[lineNum].push_back( StoneWall( height - 1, i ).UniquePtr() );
                        break;
                    case WOOD_REP:
                        map[lineNum].push_back( WoodenWall( height - 1, i ).UniquePtr() );
                        break;
                    case ENEMY_GUN_REP:
                        map[lineNum].push_back( EnemyGun( height - 1, i, playerY, playerX ).UniquePtr() );
                        enemyPos.push_back( make_pair( height - 1, i) );
                        break;
                    case ENEMY_NO_GUN_REP:
                        map[lineNum].push_back( EnemyNoGun( height - 1, i, playerY, playerX ).UniquePtr() );
                        enemyPos.push_back( make_pair( height - 1, i) );
                        break;
                    case SNIPER_REP:
                        map[lineNum].push_back( SniperGun( height - 1, i ).UniquePtr() );
                        break;
                    case SHOTGUN_REP:
                        map[lineNum].push_back( ShotGun( height - 1, i ).UniquePtr() );
                        break;
                    default:
                        valid = false;
                        break;

                }
            }
            for ( const auto &kv : enemyPos )
            {
                map[kv.first][kv.second]->SetPlayerY( playerY );
                map[kv.first][kv.second]->SetPlayerX( playerX );
            }
            width = line.size();
            ++lineNum;
            firstLine = false;
        }
        sourceFile.close();
        if ( height < MIN_HEIGHT_MAP || height > MAX_HEIGHT_MAP )
            valid = false;
    }
    else
    {
        valid = false;
    }
}

const int Map::ShootToMove( const int &dir )
{
    switch ( dir )
    {
        case SHOOT_UP:
            return UP_DIR;
        case SHOOT_DOWN:
            return DOWN_DIR;
        case SHOOT_LEFT:
            return LEFT_DIR;
        case SHOOT_RIGHT:
            return RIGHT_DIR;
        default:
            return 0;
    }
}

void Map::Move( uint32_t & y, uint32_t & x, const int & dir )
{
    Move ( y, x, dir, map[y][x]->Moves() );
}
void Map::Move( uint32_t & y, uint32_t & x, const int & dir, const int & moves )
{
    for ( int i = 0; i < moves; ++i )
    {
        uint32_t otherY = y;
        uint32_t otherX = x;
        switch ( dir )
        {
            case UP_DIR:
                --otherY;
                break;
            case DOWN_DIR:
                ++otherY;
                break;
            case LEFT_DIR:
                --otherX;
                break;
            case RIGHT_DIR:
                ++otherX;
                break;
            default:
                return;
        }

        if ( otherX >= width || otherX < 0 || otherY >= height || otherY < 0 )
            return;

        const char oldRep = map[y][x]->Rep();
        map[y][x]->Collision( map[y][x], map[otherY][otherX], score, lives, weapon );

        if ( map[otherY][otherX]->Rep() == oldRep )
        {
            y = otherY;
            x = otherX;
        }
    }
}

unique_ptr < Object > Map::ChooseBullet( const uint32_t & y, const uint32_t & x, const int & dir, const char & owner )
{
    if ( owner == PLAYER_REP )
    {
        if ( weapon == SHOTGUN_TEXT )
            return Bullet( y, x, dir, SHOTGUN_BULLET_REP, SHOTGUN_BULLET_MOVES ).UniquePtr();
        if ( weapon == SNIPERGUN_TEXT )
            return Bullet( y, x, dir, SNIPER_BULLET_REP, SNIPER_BULLET_MOVES ).UniquePtr();
        return Bullet( y, x, dir ).UniquePtr();
    }
    return Bullet( y, x, dir, ENEMY_BULLET_REP ).UniquePtr();
}

void Map::SpawnOneBullet( uint32_t y, uint32_t x, const int &dir, const char & owner )
{
    switch ( dir )
    {
        case UP_DIR:
            --y;
            break;
        case DOWN_DIR:
            ++y;
            break;
        case LEFT_DIR:
            --x;
            break;
        case RIGHT_DIR:
            ++x;
            break;
        default:
            return;
    }

    if ( x >= width || x < 0 || y >= height || y < 0 )
        return;

    unique_ptr < Object > bullet = ChooseBullet( y, x, dir, owner );
    unique_ptr <Object> bulletCpy = ChooseBullet( y, x, dir, owner );

    bullet->Collision( bulletCpy, map[y][x], score, lives, weapon );

    if ( map[y][x]->Rep() == bullet->Rep() )
        Move( y, x, dir, map[y][x]->Moves() - 1 );

    if ( map[y][x]->Rep() == bullet->Rep() )
        bulletPos.push_back( make_pair( y, x ) );
}

void Map::SpawnBullet ( const uint32_t & y, const uint32_t & x, const int & dir, const char & owner )
{
    SpawnOneBullet( y, x, dir, owner );
    if ( weapon == SHOTGUN_TEXT && owner == PLAYER_REP )
    {
        for ( uint32_t i = 1; i < ( SHOTGUN_BULLET_WIDTH + 1) / 2; ++i )
        {
            if ( dir == UP_DIR || dir == DOWN_DIR )
            {
                SpawnOneBullet( y, x + i, dir, owner );
                SpawnOneBullet( y, x - i, dir, owner );
            }
            else
            {
                SpawnOneBullet( y + i, x, dir, owner );
                SpawnOneBullet( y - i, x, dir, owner );
            }
        }
    }
}

void Map::MoveBullet()
{
    auto it = bulletPos.begin();
    while ( it != bulletPos.end() )
    {
        if ( it->first > height || it->first <= 0 || it->second > width || it->second <= 0 )
            bulletPos.erase( it );
        else if ( map[it->first][it->second]->Rep() != BULLET_REP && map[it->first][it->second]->Rep() != SNIPER_BULLET_REP &&
                  map[it->first][it->second]->Rep() != SHOTGUN_BULLET_REP && map[it->first][it->second]->Rep() != ENEMY_BULLET_REP )
            bulletPos.erase( it );
        else
        {
            Move (it->first, it->second, map[it->first][it->second]->Dir());
            ++it;
        }
    }
}

void Map::MovePlayer()
{
    int dir = map[playerY][playerX]->Dir();
    Move( playerY, playerX, dir );
    SpawnBullet( playerY, playerX, ShootToMove( dir ), PLAYER_REP );

}

void Map::MoveEnemy()
{
    auto it = enemyPos.begin();
    while ( it != enemyPos.end() )
    {
        if ( it->first > height || it->first <= 0 || it->second > width || it->second <= 0 )
            enemyPos.erase( it );
        else if ( map[it->first][it->second]->Rep() != ENEMY_GUN_REP && map[it->first][it->second]->Rep() != ENEMY_NO_GUN_REP )
            enemyPos.erase( it );
        else
        {
            map[it->first][it->second]->SetPlayerY( playerY );
            map[it->first][it->second]->SetPlayerX( playerX );
            int dir = map[it->first][it->second]->Dir();
            Move ( it->first, it->second, dir );
            SpawnBullet( it->first, it->second, ShootToMove( dir ), ENEMY_GUN_REP );
            ++it;
        }
    }
}
int Map::UpdateMap()
{
    MoveBullet();
    MovePlayer();
    MoveEnemy();
    if ( numeric_limits<int>::max() - ENEMY_GUN_KILL < score || numeric_limits<int>::max() - ENEMY_NO_GUN_KILL < score )
        return WON_GAME;
    if ( lives <= 0 )
        return LOST_GAME;
    return CONTINUE_GAME;
}

bool Map::PlaceExist( const uint32_t &y, const uint32_t &x )
{
    return ( y < height && x < width );
}

bool Map::IsEmptyYX( const uint32_t &y, const uint32_t &x )
{
    return map[y][x]->Rep() == EmptySpace(y, x).Rep();
}

bool Map::IsEmptyPlace( const uint32_t &y, const uint32_t &x, const uint16_t &lenght )
{
    if ( !IsEmptyYX( y, x ) || lenght > y || lenght > x )
        return false;
    for ( int i = 1; i <= lenght; ++i )
    {
        if ( !( PlaceExist( y - i, x ) && IsEmptyYX ( y - i, x ) && PlaceExist( y + i, x ) && IsEmptyYX ( y + i, x ) &&
             PlaceExist( y , x - i ) && IsEmptyYX ( y , x - i ) &&  PlaceExist( y , x + i ) && IsEmptyYX ( y , x + i ) &&
             PlaceExist( y - i, x - i ) && IsEmptyYX ( y - i, x - i ) && PlaceExist( y + i, x + i ) && IsEmptyYX ( y + i, x + i ) &&
             PlaceExist( y + i, x - i ) && IsEmptyYX ( y + i, x - i ) && PlaceExist( y - i, x + i ) && IsEmptyYX ( y - i, x + i ) ) )
            return false;
    }
    return true;
}

bool Map::TrySpawnYX( uint32_t &y, uint32_t &x, const uint16_t &length )
{
    uint16_t cnt = 0;
    do
    {
        x = rand() % ( width - 4 );
        y = rand() % ( height - 4 );
        ++cnt;
    }
    while ( !IsEmptyPlace( y, x, length ) && cnt < 100 );
    return cnt < 100;
}

bool Map::SpawnBonus()
{
    uint32_t x;
    uint32_t y;

    if ( !TrySpawnYX( y, x, SPAWN_BONUS_SPACE ) )
        return false;

    unique_ptr < Object > bonusN = SniperGun( y, x ).UniquePtr();
    unique_ptr < Object > bonusH = ShotGun( y, x ).UniquePtr();
    if ( rand() % 2 )
        bonusN.swap ( bonusH );
    map[y][x].swap ( bonusN );
    return true;
}

bool Map::SpawnEnemyGun()
{
    uint32_t x;
    uint32_t y;
    if ( !TrySpawnYX( y, x, SPAWN_ENEMY_SPACE ) )
        return false;
    map[y][x] = EnemyGun( y, x, playerY, playerX ).UniquePtr();
    enemyPos.push_back( make_pair( y, x ) );
    return true;
}

bool Map::SpawnEnemyNoGun()
{
    uint32_t x;
    uint32_t y;
    if ( !TrySpawnYX( y, x, SPAWN_ENEMY_SPACE ) )
        return false;
    map[y][x] = EnemyNoGun( y, x, playerY, playerX ).UniquePtr();
    enemyPos.push_back( make_pair( y, x ) );
    return true;
}

int Map::NumberOfDigits( int num ) const
{
    int digits = 0;
    if ( num == 0 )
        return 1;
    while ( num != 0 )
    {
        ++digits;
        num /= 10;
    }
    return digits;
}


void Map::PrintStatsBoard() const
{
    for ( size_t i = 0; i < width; ++i )
        cout << STATS_BOARD_TOP;
    cout << endl;

    cout << STATS_BOARD_SIDES << SCORE_PRINT << score;
    for ( size_t j = 0; j < width - ( 2 + strlen( SCORE_PRINT ) + NumberOfDigits( score ) ); ++j )
        cout << ' ';
    cout << STATS_BOARD_SIDES << endl;

    cout << STATS_BOARD_SIDES << LIVES_PRINT << lives;
    for ( size_t j = 0; j < width - ( 2 + strlen( LIVES_PRINT ) + NumberOfDigits( lives ) ); ++j )
        cout << ' ';
    cout << STATS_BOARD_SIDES << endl;

    cout << STATS_BOARD_SIDES << WEAPON_PRINT << weapon;
    for ( size_t j = 0; j < width - ( 2 + strlen( WEAPON_PRINT ) + weapon.size() ); ++j )
        cout << ' ';
    cout << STATS_BOARD_SIDES << endl;

}

void Map::PrintMap () const
{

    if ( valid )
    {
        PrintStatsBoard();
        for ( const auto &kv : map )
        {
            for ( const auto &lv : kv )
            {
                lv->Print();
            }
            cout << endl;
        }
    }
}
