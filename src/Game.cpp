//
// Created by ondrej on 5/17/20.
//

#include "Game.h"
#include <algorithm>

const int Game::Gcd( const int a, const int b )
{

    if (a == b)
        return a;

    if (a > b)
        return Gcd( a - b, b );
    return Gcd( a, b - a );
}

const int Game::Lcm( const int a, const int b )
{
    return ( a * b )/ Gcd( a, b );
}

bool Game::Play()
{
    Map map;
    if ( !map.valid )
    {
        system( "clear" );
        cout << "Trying to read map from invalid file or map is invalid" << endl;
        return false;
    }

    if ( !testmode )
    {
        for ( int i = 0; i < SPAWN_BONUS_START; ++i )
            map.SpawnBonus();
        for ( int i = 0; i < SPAWN_ENEMY_GUN_START; ++i )
            map.SpawnEnemyGun();
        for ( int i = 0; i < SPAWN_ENEMY_NO_GUN_START; ++i )
            map.SpawnEnemyNoGun();
    }

    int ticks = 1;
    bool spawnBonusNext = false;
    bool spawnEnemyGunNext = false;
    bool spawnEnemyNoGunNext = false;
    int ticksModule = Lcm( SPAWN_BONUS_RATE, Lcm( SPAWN_ENEMY_GUN_RATE, SPAWN_ENEMY_NO_GUN_RATE ) );

    while ( true )
    {
        system( "clear" );
        map.PrintMap();
        int gameResult = map.UpdateMap();

        if ( gameResult == LOST_GAME )
        {
            if ( !testmode )
            {
                if ( !ScoreBoard().Add( map.GetScore(), GAMEOVER_TEXT ) )
                {
                    system("clear");
                    cout << "Cannot write into file with scores or file does not exist" << endl;
                    return false;
                }
            }
            return true;
        }
        else if ( gameResult == WON_GAME )
        {
            if ( !testmode )
            {
                if ( !ScoreBoard().Add( map.GetScore(), VICTORY_TEXT ) )
                {
                    system("clear");
                    cout << "Cannot write into file with scores or file does not exist" << endl;
                    return false;
                }
            }
            return true;
        }

        if ( !testmode )
        {
            if ( ticks % SPAWN_BONUS_RATE == 0 || spawnBonusNext )
                spawnBonusNext = !map.SpawnBonus();

            if ( ticks % SPAWN_ENEMY_GUN_RATE == 0 || spawnEnemyGunNext )
                spawnEnemyGunNext = !map.SpawnEnemyGun();

            if ( ticks % SPAWN_ENEMY_NO_GUN_RATE == 0 || spawnEnemyNoGunNext )
                spawnEnemyNoGunNext = !map.SpawnEnemyNoGun();

            if ( ticks >= ticksModule )
                ticks = 1;
            ticks++;
        }
    }
}