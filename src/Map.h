//
// Created by ondrej on 5/13/20.
//

#ifndef SEMESTRALKA_MAP_H
#define SEMESTRALKA_MAP_H

#include "Object/Object.h"
#include "constants.h"
#include "Object/StationaryObject/EmptySpace.h"
#include "Object/StationaryObject/ShotGun.h"
#include "Object/StationaryObject/SniperGun.h"
#include "Object/StationaryObject/StoneWall.h"
#include "Object/StationaryObject/WoodenWall.h"
#include "Object/MovingObject/Player.h"
#include "Object/MovingObject/EnemyGun.h"
#include "Object/MovingObject/EnemyNoGun.h"
#include "Object/MovingObject/Bullet.h"
#include <vector>
#include <unistd.h>

using namespace std;
/**
 * Represents map on which game is played with Objects on it
 */
class Map
{
    /**
     * height of map
     */
    uint32_t height;
    /**
     * width of map
     */
    uint32_t width;
    /**
     * current score
     */
    int score;
    /**
     * amount of lives left
     */
    int lives;
    /**
     * currently equipped weapon
     */
    string weapon;
    /**
     * Player x coordinate
     */
    uint32_t playerX;
    /**
     * Player y coordinate
     */
    uint32_t playerY;
    /**
     * map itself
     */
    vector < vector < unique_ptr < Object > > > map;
    /**
     * Positions of enemies on map
     */
    vector < pair < uint32_t, uint32_t > > enemyPos;
    /**
     * Positions of bullet on map
     */
    vector < pair < uint32_t, uint32_t > > bulletPos;
    /**
     * Prints board above map ( current score, lives and currently equipped weapon )
     */
    void PrintStatsBoard () const;
    /**
     * How many digits given number has
     * @param num given number
     * @return number of digits in this number
     */
    int NumberOfDigits ( int num ) const;
    /**
     * Moves Bullets on map
     * If Bullet no longer exists on map if is erased from variable bulletPos
     */
    void MoveBullet();
    /**
     * Moves Player on map
     */
    void MovePlayer();
    /**
     * Moves enemies ( EnemyNoGun, EnemyGun ) on map
     * If enemy no longer exists on map is is erased from variable enemyPos
     */
    void MoveEnemy();
    /**
     * Changes direction of shooting to direction of movement
     * ( this function is called when enemy or player shoots )
     * @param dir direction of shooting
     * @return direction of movement
     */
    const int ShootToMove ( const int & dir );
    /**
     * Calls move with 4 parameters
     */
    void Move ( uint32_t & y, uint32_t & x, const int & dir );
    /**
     * Moves Object on given y and x coordinates on map in given direction
     * It moves this Object one place ( field ) on map as many times as the value of variable moves is equal to
     * @param y y coordinate of Object that is trying to move
     * @param x x coordinate of Object that is trying to move
     * @param dir direction of movement
     * @param moves how many fields should Object move in one game tick ( how many times it moves one place in given direction )
     */
    void Move ( uint32_t & y, uint32_t & x, const int & dir, const int & moves );
    /**
     * Based on given parameters return what type of bullet is being shot
     * @param y y coordinate where the Bullet will spawn
     * @param x x coordinate where the Bullet will spawn
     * @param dir direction in which Bullet will travel
     * @param owner who shot this Bullet ( Player or EnemyGun )
     * @return type of Bullet that is being shot
     */
    unique_ptr < Object > ChooseBullet ( const uint32_t & y, const uint32_t & x, const int & dir, const char & owner );
    /**
     * Spawns single or multiple Bullets on map ( multiple in case on ShotGun is equipped )
     * @param y y coordinate where I want to spawn Bullet
     * @param x x coordinate where I want to spawn Bullet
     * @param dir direction in which bullet will be shot
     * @param owner who shot this Bullet ( Player or EnemyGun )
     */
    void SpawnBullet ( const uint32_t & y, const uint32_t & x, const int & dir, const char & owner );
    /**
     * Spawns single Bullet on map ( this method is being called from method SpawnBullet )
     * @param y y coordinate where I want to spawn Bullet
     * @param x x coordinate where I want to spawn Bullet
     * @param dir direction in which bullet will be shot
     * @param owner who shot this Bullet ( Player or EnemyGun )
     */
    void SpawnOneBullet ( uint32_t y, uint32_t x, const int &dir, const char & owner );
    /**
     * Check if it is possible to spawn Object on map on given x and y coordinates
     * If its not possible it tries to change x and y coordinates so it can spawn
     * @param y y coordinate of where I want to spawn
     * @param x x coordinate of where I want to spawn
     * @param length distance around the spawned Object on map that needs to be EmptySpace
     * @return true if i can spawn Object on given x and y coordinates
     */
    bool TrySpawnYX( uint32_t & y, uint32_t & x, const uint16_t &length );
    /**
     * Check if place with given coordinates and its surroundings is occupied with EmptySpace
     * @param y y coordinates
     * @param x x coordinates
     * @param length how many places ( fields on map ) need to be Empty around map[y][x] to return true
     * @return true if on x and y coordinates on map, and also on its surrounding is EmptySpace
     */
    bool IsEmptyPlace( const uint32_t &y, const uint32_t &x, const uint16_t &length );
    /**
     * Check if place with given coordinates is occupied with EmptySpace
     * @param y y coordinate
     * @param x x coordinate
     * @return true if there is EmptySpace on this place on map
     */
    bool IsEmptyYX( const uint32_t &y, const uint32_t &x );
    /**
     * Checks if place with given coordinates from parameters exists on map
     * @param y y coordinates
     * @param x x coordinates
     * @return true if place exists ( false if not )
     */
    bool PlaceExist ( const uint32_t &y, const uint32_t &x );

public:
    /**
     * value is true if map is valid ( file from which i read is valid, etc. )
     */
    bool valid;
    /**
     * Constructor
     * @param source file from which im reading map
     */
    Map ( const char * source = DEFAULT_MAP );
    /**
     * Prints map with current position of Objects on map
     */
    void PrintMap () const;
    /**
     * Updates map - tries to move all MovableObject on map
     * @return one of three types of value based on situation - game was lost, won or Player is alive and had not achieve a score of INT_MAX
     */
    int UpdateMap ();
    /**
     * Spawns bonus ( SniperGun or ShotGun ) on map ( chooses which one using rand() )
     * @return true if bonus was spawned
     */
    bool SpawnBonus ();
    /**
     * Spawns EnemyGun on map
     * @return true if EnemyGun was spawned
     */
    bool SpawnEnemyGun();
    /**
     * Spawns EnemyNoGun on map
     * @return true if EnemyNoGun was spawned
     */
    bool SpawnEnemyNoGun();
    /**
     * Getter for current score
     * @return score
     */
    const int GetScore() const { return score; }
};

#endif //SEMESTRALKA_MAP_H
