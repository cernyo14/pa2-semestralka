//
// Created by ondrej on 5/18/20.
//

#ifndef SEMESTRALKA_SCOREBOARD_H
#define SEMESTRALKA_SCOREBOARD_H

#include "constants.h"
#include "Input.h"
#include <iostream>
#include <vector>

using namespace std;
/**
 * Compares two pair of score and name ( who set this score ), firstly by score ( order from highest to lowest ) and secondary by name ( alphabetically )
 */
struct ScoresComparator
{
    bool operator () ( const pair < int, string > &a, const pair < int, string > &b )
    {
        if ( a.first != b.first )
            return b.first < a.first;
        return a.second < b.second;
    }
};
/**
 * Represents Scoreboard ( top number of scores )
 */
class ScoreBoard
{
    /**
     * vector in which top scores are temporarily saved
     */
    vector < pair < int , string > > scoreboard;
    /**
     * file in which top scores are stored
     */
    const char * file;
    /**
     * Transforms number in string to integer
     * @param str is given number in a string form
     * @return number in a integer form
     */
    const int StringToInt( string str );
    /**
     * Read top scores from file, sorts them and saves them in vector of pairs called scoreboard
     * @return false it was unable to read from given file ( otherwise returns true )
     */
    bool ReadAndSave();
    /**
     * Gets users name after end of game
     * @param text is text which will be printed after end of game ( either victory text or game over text )
     * @return name entered by user into keyboard
     */
    string GetName( const char * text );
    /**
     * Gets length of first line of text printed above scoreboard
     * @param text is text printed above
     * @return how many chars first line of this text has
     */
    const uint64_t GetLineLength( const char * text ) const;
    /**
     * How many digits given number has
     * @param num given number
     * @return number of digits in this number
     */
    int NumberOfDigits( int num ) const;
public:
    /**
     * Constructor
     * @param file file with top scores
     */
    ScoreBoard( const char * file = HIGHSCORE_FILE ) : file ( file ) { }
    /**
     * Read and Print top scores from best to worst
     * @return false if error occurred ( i.e. file with scores does not exist )
     */
    bool Read();
    /**
     * Adds score to file with top scores
     * First it calls GetName ( gets current players name ), then Adds score and name of the current player to file, then
     * calls ReadAndSave method ( which sorts and inputs scores into variable scoreboard )
     * and then it removes all scores from file and inputs scores from variable scoreboard into given file
     * @param score score we want to add
     * @param text is text which will be printed after end of game ( either victory text or game over text )
     * @return false if error occurred ( i.e. file with scores does not exist )
     */
    bool Add( const int score, const char * text = GAMEOVER_TEXT );
};
#endif //SEMESTRALKA_SCOREBOARD_H
