//
// Created by ondrej on 5/18/20.
//

#ifndef SEMESTRALKA_INPUT_H
#define SEMESTRALKA_INPUT_H
/**
 * Gets input from keyboard without need to press Enter
 */
class Input
{
public:
    /**
     * Reads input from keyboard without the need of pressing Enter after inserting input
     * getchar without needing to press Enter
     * this method is from StackOverflow
     * @return
     */
    const int GetInput() const;

};
#endif //SEMESTRALKA_INPUT_H
