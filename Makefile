
CXX=g++
CXXFLAGS=-std=c++17 -Wall -pedantic -g -c

LD=g++
LDFLAGS=-g -o

OBJ=main.o Map.o Menu.o ScoreBoard.o Input.o Game.o Object.o StationaryObject.o EmptySpace.o ShotGun.o SniperGun.o StoneWall.o WoodenWall.o MovingObject.o Player.o EnemyGun.o EnemyNoGun.o Bullet.o
EXECUTABLE=cernyo14
DOC=doc/

.PHONY: compile run doc clean all

all: run doc clean

run: compile
	./$(EXECUTABLE)

compile: $(OBJ)
	$(LD) $(LDFLAGS) $(EXECUTABLE) $(OBJ)

clean:
	rm -r $(OBJ) $(EXECUTABLE)
	rm -fr $(DOC)

doc: src/main.cpp src/Menu.h src/Map.h src/Input.h src/ScoreBoard.h src/Object/Object.h src/Object/MovingObject/MovingObject.h src/Object/MovingObject/Player.h src/Object/MovingObject/EnemyGun.h src/Object/MovingObject/EnemyNoGun.h src/Object/MovingObject/Bullet.h src/Object/StationaryObject/StationaryObject.h src/Object/StationaryObject/EmptySpace.h src/Object/StationaryObject/ShotGun.h src/Object/StationaryObject/SniperGun.h src/Object/StationaryObject/StoneWall.h src/Object/StationaryObject/WoodenWall.h
	doxygen Doxyfile
	

Bullet.o: src/Object/MovingObject/Bullet.cpp \
 src/Object/MovingObject/Bullet.h src/Object/MovingObject/MovingObject.h \
 src/Object/MovingObject/../Object.h \
 src/Object/MovingObject/../../constants.h \
 src/Object/MovingObject/../StationaryObject/EmptySpace.h \
 src/Object/MovingObject/../StationaryObject/StationaryObject.h \
 src/Object/MovingObject/../StationaryObject/../Object.h
	$(CXX) $(CXXFLAGS) src/Object/MovingObject/Bullet.cpp

EnemyGun.o: src/Object/MovingObject/EnemyGun.cpp \
 src/Object/MovingObject/EnemyGun.h \
 src/Object/MovingObject/MovingObject.h \
 src/Object/MovingObject/../Object.h \
 src/Object/MovingObject/../../constants.h \
 src/Object/MovingObject/../StationaryObject/EmptySpace.h \
 src/Object/MovingObject/../StationaryObject/StationaryObject.h \
 src/Object/MovingObject/../StationaryObject/../Object.h
	$(CXX) $(CXXFLAGS) src/Object/MovingObject/EnemyGun.cpp

EnemyNoGun.o: src/Object/MovingObject/EnemyNoGun.cpp \
 src/Object/MovingObject/EnemyNoGun.h \
 src/Object/MovingObject/MovingObject.h \
 src/Object/MovingObject/../Object.h \
 src/Object/MovingObject/../../constants.h \
 src/Object/MovingObject/../StationaryObject/EmptySpace.h \
 src/Object/MovingObject/../StationaryObject/StationaryObject.h \
 src/Object/MovingObject/../StationaryObject/../Object.h
	$(CXX) $(CXXFLAGS) src/Object/MovingObject/EnemyNoGun.cpp

Player.o: src/Object/MovingObject/Player.cpp \
 src/Object/MovingObject/Player.h src/Object/MovingObject/MovingObject.h \
 src/Object/MovingObject/../Object.h \
 src/Object/MovingObject/../../constants.h \
 src/Object/MovingObject/../StationaryObject/EmptySpace.h \
 src/Object/MovingObject/../StationaryObject/StationaryObject.h \
 src/Object/MovingObject/../StationaryObject/../Object.h \
 src/Object/MovingObject/../StationaryObject/ShotGun.h \
 src/Object/MovingObject/../StationaryObject/SniperGun.h \
 src/Object/MovingObject/../StationaryObject/StoneWall.h \
 src/Object/MovingObject/../StationaryObject/WoodenWall.h \
 src/Object/MovingObject/../../Input.h src/Object/MovingObject/EnemyGun.h \
 src/Object/MovingObject/EnemyNoGun.h
	$(CXX) $(CXXFLAGS) src/Object/MovingObject/Player.cpp

MovingObject.o: src/Object/MovingObject/MovingObject.cpp \
 src/Object/MovingObject/MovingObject.h \
 src/Object/MovingObject/../Object.h \
 src/Object/MovingObject/../../constants.h \
 src/Object/MovingObject/../StationaryObject/EmptySpace.h \
 src/Object/MovingObject/../StationaryObject/StationaryObject.h \
 src/Object/MovingObject/../StationaryObject/../Object.h
	$(CXX) $(CXXFLAGS) src/Object/MovingObject/MovingObject.cpp

StationaryObject.o: src/Object/StationaryObject/StationaryObject.cpp \
 src/Object/StationaryObject/StationaryObject.h \
 src/Object/StationaryObject/../Object.h \
 src/Object/StationaryObject/../../constants.h
	$(CXX) $(CXXFLAGS) src/Object/StationaryObject/StationaryObject.cpp

EmptySpace.o: src/Object/StationaryObject/EmptySpace.cpp \
 src/Object/StationaryObject/EmptySpace.h \
 src/Object/StationaryObject/StationaryObject.h \
 src/Object/StationaryObject/../Object.h \
 src/Object/StationaryObject/../../constants.h
	$(CXX) $(CXXFLAGS) src/Object/StationaryObject/EmptySpace.cpp

ShotGun.o: src/Object/StationaryObject/ShotGun.cpp \
 src/Object/StationaryObject/ShotGun.h \
 src/Object/StationaryObject/StationaryObject.h \
 src/Object/StationaryObject/../Object.h \
 src/Object/StationaryObject/../../constants.h
	$(CXX) $(CXXFLAGS) src/Object/StationaryObject/ShotGun.cpp

SniperGun.o: src/Object/StationaryObject/SniperGun.cpp \
 src/Object/StationaryObject/SniperGun.h \
 src/Object/StationaryObject/StationaryObject.h \
 src/Object/StationaryObject/../Object.h \
 src/Object/StationaryObject/../../constants.h
	$(CXX) $(CXXFLAGS) src/Object/StationaryObject/SniperGun.cpp

StoneWall.o: src/Object/StationaryObject/StoneWall.cpp \
 src/Object/StationaryObject/StoneWall.h \
 src/Object/StationaryObject/StationaryObject.h \
 src/Object/StationaryObject/../Object.h \
 src/Object/StationaryObject/../../constants.h
	$(CXX) $(CXXFLAGS) src/Object/StationaryObject/StoneWall.cpp

WoodenWall.o: src/Object/StationaryObject/WoodenWall.cpp \
 src/Object/StationaryObject/WoodenWall.h \
 src/Object/StationaryObject/StationaryObject.h \
 src/Object/StationaryObject/../Object.h \
 src/Object/StationaryObject/../../constants.h
	$(CXX) $(CXXFLAGS) src/Object/StationaryObject/WoodenWall.cpp

Object.o: src/Object/Object.cpp src/Object/Object.h \
 src/Object/../constants.h
	$(CXX) $(CXXFLAGS) src/Object/Object.cpp

Game.o: src/Game.cpp src/Game.h src/Map.h src/Object/Object.h \
 src/Object/../constants.h src/constants.h \
 src/Object/StationaryObject/EmptySpace.h \
 src/Object/StationaryObject/StationaryObject.h \
 src/Object/StationaryObject/../Object.h \
 src/Object/StationaryObject/ShotGun.h \
 src/Object/StationaryObject/SniperGun.h \
 src/Object/StationaryObject/StoneWall.h \
 src/Object/StationaryObject/WoodenWall.h \
 src/Object/MovingObject/Player.h src/Object/MovingObject/MovingObject.h \
 src/Object/MovingObject/../Object.h \
 src/Object/MovingObject/../StationaryObject/EmptySpace.h \
 src/Object/MovingObject/../StationaryObject/ShotGun.h \
 src/Object/MovingObject/../StationaryObject/SniperGun.h \
 src/Object/MovingObject/../StationaryObject/StoneWall.h \
 src/Object/MovingObject/../StationaryObject/WoodenWall.h \
 src/Object/MovingObject/../../Input.h src/Object/MovingObject/EnemyGun.h \
 src/Object/MovingObject/EnemyNoGun.h src/Object/MovingObject/EnemyGun.h \
 src/Object/MovingObject/EnemyNoGun.h src/Object/MovingObject/Bullet.h \
 src/ScoreBoard.h src/Input.h
	$(CXX) $(CXXFLAGS) src/Game.cpp

ScoreBoard.o: src/ScoreBoard.cpp src/ScoreBoard.h src/constants.h \
 src/Input.h
	$(CXX) $(CXXFLAGS) src/ScoreBoard.cpp

Input.o: src/Input.cpp src/Input.h
	$(CXX) $(CXXFLAGS) src/Input.cpp

Menu.o: src/Menu.cpp src/./Menu.h src/./constants.h src/./Input.h
	$(CXX) $(CXXFLAGS) src/Menu.cpp

Map.o: src/Map.cpp src/./Map.h src/./Object/Object.h \
 src/./Object/../constants.h src/./constants.h \
 src/./Object/StationaryObject/EmptySpace.h \
 src/./Object/StationaryObject/StationaryObject.h \
 src/./Object/StationaryObject/../Object.h \
 src/./Object/StationaryObject/ShotGun.h \
 src/./Object/StationaryObject/SniperGun.h \
 src/./Object/StationaryObject/StoneWall.h \
 src/./Object/StationaryObject/WoodenWall.h \
 src/./Object/MovingObject/Player.h \
 src/./Object/MovingObject/MovingObject.h \
 src/./Object/MovingObject/../Object.h \
 src/./Object/MovingObject/../StationaryObject/EmptySpace.h \
 src/./Object/MovingObject/../StationaryObject/ShotGun.h \
 src/./Object/MovingObject/../StationaryObject/SniperGun.h \
 src/./Object/MovingObject/../StationaryObject/StoneWall.h \
 src/./Object/MovingObject/../StationaryObject/WoodenWall.h \
 src/./Object/MovingObject/../../Input.h \
 src/./Object/MovingObject/EnemyGun.h \
 src/./Object/MovingObject/EnemyNoGun.h \
 src/./Object/MovingObject/EnemyGun.h \
 src/./Object/MovingObject/EnemyNoGun.h \
 src/./Object/MovingObject/Bullet.h
	$(CXX) $(CXXFLAGS) src/Map.cpp

main.o: src/main.cpp src/Game.h src/Map.h src/Object/Object.h \
 src/Object/../constants.h src/constants.h \
 src/Object/StationaryObject/EmptySpace.h \
 src/Object/StationaryObject/StationaryObject.h \
 src/Object/StationaryObject/../Object.h \
 src/Object/StationaryObject/ShotGun.h \
 src/Object/StationaryObject/SniperGun.h \
 src/Object/StationaryObject/StoneWall.h \
 src/Object/StationaryObject/WoodenWall.h \
 src/Object/MovingObject/Player.h src/Object/MovingObject/MovingObject.h \
 src/Object/MovingObject/../Object.h \
 src/Object/MovingObject/../StationaryObject/EmptySpace.h \
 src/Object/MovingObject/../StationaryObject/ShotGun.h \
 src/Object/MovingObject/../StationaryObject/SniperGun.h \
 src/Object/MovingObject/../StationaryObject/StoneWall.h \
 src/Object/MovingObject/../StationaryObject/WoodenWall.h \
 src/Object/MovingObject/../../Input.h src/Object/MovingObject/EnemyGun.h \
 src/Object/MovingObject/EnemyNoGun.h src/Object/MovingObject/EnemyGun.h \
 src/Object/MovingObject/EnemyNoGun.h src/Object/MovingObject/Bullet.h \
 src/ScoreBoard.h src/Input.h src/Menu.h
	$(CXX) $(CXXFLAGS) src/main.cpp


